package com.company.wellapp.models.dto;

import com.company.wellapp.models.entity.Equipment;
import com.company.wellapp.models.entity.Well;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// DTO class for Well Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class WellDto {
    private int wellId;
    private String wellName;
    List<EquipmentDto> equipmentList = new ArrayList<>();

    public static WellDto from(Well well) {
        return WellDto.builder()
                .wellId(well.getWellId())
                .wellName(well.getWellName())
                .build();
    }

    public static List<WellDto> from(List<Well> wells) {
        return wells.stream().map(WellDto::from).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Well {" +
                "well id = " + wellId +
                ", well name = '" + wellName + '\'' +
                ", equipment list = " + equipmentList +
                '}';
    }
}
