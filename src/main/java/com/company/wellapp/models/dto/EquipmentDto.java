package com.company.wellapp.models.dto;

import com.company.wellapp.models.entity.Equipment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

// DTO class Equipment Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EquipmentDto {
    private int equipmentId;
    private String equipmentName;
    private int wellId;

    public static EquipmentDto from(Equipment equipment) {
        return EquipmentDto.builder()
                .equipmentId(equipment.getEquipmentId())
                .equipmentName(equipment.getEquipmentName())
                .wellId(equipment.getWellId())
                .build();
    }

    public static List<EquipmentDto> from(List<Equipment> equipments) {
        return equipments.stream().map(EquipmentDto::from).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Equipment {" +
                "equipment Id = " + equipmentId +
                ", equipment name = '" + equipmentName + '\'' +
                '}';
    }
}
