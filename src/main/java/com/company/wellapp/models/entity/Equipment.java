package com.company.wellapp.models.entity;

import com.company.wellapp.models.dto.EquipmentDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Model class for Equipment Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Equipment {
    private int equipmentId;
    private String equipmentName;
    private int wellId;

    public static Equipment from(EquipmentDto equipmentDto) {
        return Equipment.builder()
                .equipmentId(equipmentDto.getEquipmentId())
                .equipmentName(equipmentDto.getEquipmentName())
                .wellId(equipmentDto.getWellId())
                .build();
    }
}
