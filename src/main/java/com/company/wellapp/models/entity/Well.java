package com.company.wellapp.models.entity;

import com.company.wellapp.models.dto.WellDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

//Model class for Well Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Well {
    private int wellId;
    private String wellName;

    List<Equipment> equipmentList = new ArrayList<>();

    public static Well from(WellDto wellDto) {
        return Well.builder()
                .wellId(wellDto.getWellId())
                .wellName(wellDto.getWellName())
                .build();
    }
}
