package com.company.wellapp.handlers;

import java.util.Scanner;

public interface InputHandler {
    void start(Scanner scanner);
}
