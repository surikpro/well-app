package com.company.wellapp.handlers;

import com.company.wellapp.models.dto.WellDto;
import com.company.wellapp.services.EquipmentService;
import com.company.wellapp.services.WellsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

//Provides details about well
@Component
public class WellAndEquipmentDetailsHandler implements InputHandler {
    private final WellsService wellsService;
    private final EquipmentService equipmentService;

    @Autowired
    public WellAndEquipmentDetailsHandler(WellsService wellsService, EquipmentService equipmentService) {
        this.wellsService = wellsService;
        this.equipmentService = equipmentService;
    }


    @Override
    public void start(Scanner scanner) {
        System.out.println("Enter NAME of the WELL(s) to get information about its EQUIPMENT \n" +
                "USE SPACE between name of the wells. For example: AAA or AAA BBB");
        String[] data = scanner.nextLine().split(" ");
        for (int i = 0; i < data.length; i++) {
            WellDto wellDto = wellsService.findWellByName(data[i]);
            wellDto.setEquipmentList(equipmentService.findByWellId(wellDto.getWellId()));
            System.out.println(wellDto);
        }
    }
}
