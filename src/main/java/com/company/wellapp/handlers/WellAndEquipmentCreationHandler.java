package com.company.wellapp.handlers;

import com.company.wellapp.exceptions.CustomArrayIndexOutOfBoundsException;
import com.company.wellapp.exceptions.CustomNumberFormatException;
import com.company.wellapp.generators.Generator;
import com.company.wellapp.models.dto.EquipmentDto;
import com.company.wellapp.models.dto.WellDto;
import com.company.wellapp.services.EquipmentService;
import com.company.wellapp.services.WellsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

// Provides creation of wells and equipment
@Component
public class WellAndEquipmentCreationHandler implements InputHandler {
    private final WellsService wellsService;
    private final EquipmentService equipmentService;
    private final Generator generator;
    private final int GENERATED_NAME_lENGTH = 5;
    private final String EQUIPMENT_CREATION_MESSAGE = "Created: ";
    private final String WELL_AND_EQUIPMENT_CREATION_MESSAGE = "Created Well and Equipment: ";

    @Autowired
    public WellAndEquipmentCreationHandler(WellsService wellsService,
                                           EquipmentService equipmentService,
                                           Generator generator) {
        this.wellsService = wellsService;
        this.equipmentService = equipmentService;
        this.generator = generator;
    }

    @Override
    public void start(Scanner scanner) {
        System.out.println("Enter the NAME of the WELL and QUANTITY of EQUIPMENT \n" +
                "(between WELL NAME and EQUIPMENT QUANTITY use SPACE ' '). For Example: AAA 2");
        String text = scanner.nextLine();
        String[] data = text.split(" ");
        if (data.length > 2) {
            throw new CustomArrayIndexOutOfBoundsException();
        }
        String wellName = data[0];
        int amountOfEquipment = 0;
        try {
            amountOfEquipment = Integer.parseInt(data[1]);
        } catch (NumberFormatException e) {
            throw new CustomNumberFormatException();
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new CustomArrayIndexOutOfBoundsException();
        }
        WellDto foundWellName = wellsService.findWellByName(wellName);
        if (foundWellName != null) {
            for (int i = 0; i < amountOfEquipment; i++) {
                EquipmentDto equipmentDto = new EquipmentDto();
                equipmentDto.setEquipmentName(generator.generate(GENERATED_NAME_lENGTH));
                equipmentDto.setWellId(wellsService.findWellByName(wellName).getWellId());
                equipmentService.save(equipmentDto);
                System.out.println(EQUIPMENT_CREATION_MESSAGE + equipmentService.findByName(equipmentDto.getEquipmentName()));
            }
        } else {
            WellDto wellDto = new WellDto();
            EquipmentDto equipmentDto = new EquipmentDto();
            wellDto.setWellName(wellName);
            wellsService.save(wellDto);
            WellDto createdWell = wellsService.findWellByName(wellName);
            for (int i = 0; i < amountOfEquipment; i++) {
                equipmentDto.setEquipmentName(generator.generate(GENERATED_NAME_lENGTH));
                equipmentDto.setWellId(createdWell.getWellId());
                equipmentService.save(equipmentDto);
            }
            createdWell.setEquipmentList(equipmentService.findByWellId(createdWell.getWellId()));
            System.out.println(WELL_AND_EQUIPMENT_CREATION_MESSAGE + createdWell + "\n" + " and " +
                    equipmentService.findByName(equipmentDto.getEquipmentName()));
        }
    }
}
