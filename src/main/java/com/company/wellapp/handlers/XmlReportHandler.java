package com.company.wellapp.handlers;

import com.company.wellapp.mappers.XmlSerializer;
import com.company.wellapp.services.EquipmentService;
import com.company.wellapp.services.WellsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

// This bean for handling XML-reports
@Component
public class XmlReportHandler implements InputHandler {
    private final WellsService wellsService;
    private final EquipmentService equipmentService;
    private final XmlSerializer xmlSerializer;

    @Autowired
    public XmlReportHandler(WellsService wellsService,
                            EquipmentService equipmentService,
                            XmlSerializer xmlSerializer) {
        this.wellsService = wellsService;
        this.equipmentService = equipmentService;
        this.xmlSerializer = xmlSerializer;
    }


    @Override
    public void start(Scanner scanner) {
        System.out.println("Enter file name to download: ");
        String line = scanner.nextLine();
        xmlSerializer.serializeToXml(line);
    }
}
