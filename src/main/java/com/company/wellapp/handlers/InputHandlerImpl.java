package com.company.wellapp.handlers;

import com.company.wellapp.services.WellsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;
//This bean provides options for choice according to requirements
@Component
public class InputHandlerImpl implements InputHandler {
    private final WellsService wellsService;
    private final WellAndEquipmentCreationHandler wellAndEquipmentCreationHandler;
    private final WellAndEquipmentDetailsHandler wellAndEquipmentDetailsHandler;
    private final XmlReportHandler xmlReportHandler;
    private final String OPTION_MESSAGE = "Please enter the options: \n " +
            "1 - enter option 1 to find and create the \n " +
            "name of the well and amount of equipment \n " +
            "2 - enter option 2 to find out about the \n " +
            "name of the well(s) and its equipment details \n " +
            "3 - export all data about wells and their equipment \n ";
    private final String START_APP_MESSAGE = "Start of application. Yahoo!";

    @Autowired
    public InputHandlerImpl(WellsService wellsService,
                            WellAndEquipmentCreationHandler wellAndEquipmentCreationHandler,
                            WellAndEquipmentDetailsHandler wellAndEquipmentDetailsHandler,
                            XmlReportHandler xmlReportHandler) {
        this.wellsService = wellsService;
        this.wellAndEquipmentCreationHandler = wellAndEquipmentCreationHandler;
        this.wellAndEquipmentDetailsHandler = wellAndEquipmentDetailsHandler;
        this.xmlReportHandler = xmlReportHandler;
    }

    @Override
    public void start(Scanner scanner) {
        System.out.println(START_APP_MESSAGE);
        System.out.println(OPTION_MESSAGE);
        String text = scanner.nextLine();
        while (!text.equals("exit")) {
            int optionNumber = Integer.parseInt(text);
            switch (optionNumber) {
                case 1 : wellAndEquipmentCreationHandler.start(scanner);
                break;
                case 2 : wellAndEquipmentDetailsHandler.start(scanner);
                break;
                case 3 : xmlReportHandler.start(scanner);
                break;
            }
            System.out.println(OPTION_MESSAGE);
            text = scanner.nextLine();
        }
    }
}
