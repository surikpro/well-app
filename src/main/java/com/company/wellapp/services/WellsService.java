package com.company.wellapp.services;

import com.company.wellapp.models.dto.WellDto;

import java.util.List;

public interface WellsService {
    void save(WellDto wellDto);
    WellDto findWellByName(String wellName);
    List<WellDto> getAllWells();
}
