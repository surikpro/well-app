package com.company.wellapp.services;

import com.company.wellapp.models.dto.EquipmentDto;
import com.company.wellapp.models.entity.Equipment;
import com.company.wellapp.repositories.EquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

// Service class for Equipment Entity
@Component
public class EquipmentServiceImpl implements EquipmentService {
    private final EquipmentRepository equipmentRepository;

    @Autowired
    public EquipmentServiceImpl(EquipmentRepository equipmentRepository) {
        this.equipmentRepository = equipmentRepository;
    }

    @Override
    public void save(EquipmentDto equipmentDto) {
        equipmentRepository.save(Equipment.from(equipmentDto));
    }

    @Override
    public List<EquipmentDto> findByWellId(int wellId) {
        List<Equipment> equipments = equipmentRepository.findByWellId(wellId);
        List<EquipmentDto> equipmentDtoList;
        if (equipments != null) {
            equipmentDtoList = EquipmentDto.from(equipments);
        } else {
            equipmentDtoList = null;
        }
        return equipmentDtoList;
    }

    @Override
    public EquipmentDto findByName(String name) {
        return EquipmentDto.from(equipmentRepository.findByName(name));
    }
}
