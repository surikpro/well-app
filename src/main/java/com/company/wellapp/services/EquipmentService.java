package com.company.wellapp.services;

import com.company.wellapp.models.dto.EquipmentDto;
import com.company.wellapp.models.entity.Equipment;

import java.util.List;

public interface EquipmentService {
    void save(EquipmentDto equipmentDto);
    List<EquipmentDto> findByWellId(int wellId);
    EquipmentDto findByName(String name);
}
