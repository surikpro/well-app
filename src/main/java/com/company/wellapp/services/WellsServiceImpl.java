package com.company.wellapp.services;

import com.company.wellapp.models.dto.WellDto;
import com.company.wellapp.models.entity.Well;
import com.company.wellapp.repositories.WellsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

// Service class for Well Entity
@Component
public class WellsServiceImpl implements WellsService {
    private final WellsRepository wellsRepository;

    @Autowired
    public WellsServiceImpl(WellsRepository wellsRepository) {
        this.wellsRepository = wellsRepository;
    }

    @Override
    public void save(WellDto wellDto) {
        wellsRepository.save(Well.from(wellDto));
    }

    @Override
    public WellDto findWellByName(String wellName) {
        Well well = wellsRepository.findWellByName(wellName);
        WellDto wellDto;
        if (well != null) {
            wellDto = WellDto.from(well);
        } else {
            wellDto = null;
        }
        return wellDto;
    }

    @Override
    public List<WellDto> getAllWells() {
        List<WellDto> wellDtos = WellDto.from(wellsRepository.findAllWells());
        return wellDtos;
    }
}
