package com.company.wellapp.configuration;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Scanner;

// Configuration class for application
@Component
@ComponentScan(value = "com.company")
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfiguration {
    private final Environment environment;

    @Autowired
    public ApplicationConfiguration(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public DataSource dataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(environment.getProperty("jdbc.url"));
        hikariConfig.setDriverClassName(environment.getProperty("jdbc.driver"));
//        Properties for PostgreSQL
//        hikariConfig.setUsername(environment.getProperty("db.user"));
//        hikariConfig.setPassword(environment.getProperty("db.password"));
        return hikariConfig;
    }

    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }
}
