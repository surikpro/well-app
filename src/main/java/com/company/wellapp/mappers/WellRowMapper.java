package com.company.wellapp.mappers;

import com.company.wellapp.models.entity.Well;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

// Object Mapper class for Well model
@Component
public class WellRowMapper implements RowMapper<Well> {
    @Override
    public Well mapRow(ResultSet rs, int rowNum) throws SQLException {
        Well well = new Well();
        well.setWellId(rs.getInt("id"));
        well.setWellName(rs.getString("name"));
        return well;
    }
}
