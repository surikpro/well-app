package com.company.wellapp.mappers;

import com.company.wellapp.models.dto.WellDto;
import com.company.wellapp.models.entity.Well;
import com.company.wellapp.services.EquipmentService;
import com.company.wellapp.services.WellsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
// Serializes POJO into XML-format
@Component
public class XmlSerializer {
    private final String PATH_TO_FILE = "C:\\Users\\aydar\\Desktop\\well-app\\src\\main\\resources\\";
    private final WellsService wellsService;
    private final EquipmentService equipmentService;
    @Autowired
    public XmlSerializer(WellsService wellsService,
                         EquipmentService equipmentService) {
        this.wellsService = wellsService;
        this.equipmentService = equipmentService;
    }

    public void serializeToXml(String line) {
        try {
            XmlMapper xmlMapper = new XmlMapper();
            List<WellDto> wellList = wellsService.getAllWells();
            for (int i = 0; i < wellList.size(); i++) {
                WellDto wellDto = wellList.get(i);
                wellDto.setEquipmentList(equipmentService.findByWellId(wellDto.getWellId()));
            }

            String xmlString = xmlMapper.writeValueAsString(wellList);

            System.out.println(xmlString);
            File xmlOutput = new File(PATH_TO_FILE + line);
            FileWriter fileWriter = new FileWriter(xmlOutput);
            fileWriter.write(xmlString);
            fileWriter.close();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
