package com.company.wellapp.mappers;

import com.company.wellapp.models.entity.Equipment;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

// Object Mapper class for Equipment model
@Component
public class EquipmentRowMapper implements RowMapper<Equipment> {
    @Override
    public Equipment mapRow(ResultSet rs, int rowNum) throws SQLException {
        Equipment equipment = new Equipment();
        equipment.setEquipmentId(rs.getInt("equipment_id"));
        equipment.setEquipmentName(rs.getString("equipment_name"));
        equipment.setWellId(rs.getInt("well_id"));
        return equipment;
    }
}
