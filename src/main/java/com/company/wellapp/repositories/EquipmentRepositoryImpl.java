package com.company.wellapp.repositories;

import com.company.wellapp.mappers.EquipmentRowMapper;
import com.company.wellapp.models.entity.Equipment;
import com.company.wellapp.models.entity.Well;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

// DAO class for Equipment Entity
@Component
public class EquipmentRepositoryImpl implements EquipmentRepository {
    private final String SQL_INSERT = "INSERT INTO equipment(equipment_name, well_id) " +
            "VALUES (?, ?)";
    private final String SQL_FIND_BY_WELL_ID = "SELECT equipment_id, equipment_name, well_id FROM equipment WHERE well_id = ?";
    private final String SQL_FIND_BY_NAME = "SELECT equipment_id, equipment_name, well_id FROM equipment WHERE equipment_name = ?";
    private final JdbcTemplate jdbcTemplate;
    private final EquipmentRowMapper equipmentRowMapper;

    @Autowired
    public EquipmentRepositoryImpl(DataSource dataSource,
                                   EquipmentRowMapper equipmentRowMapper) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.equipmentRowMapper = equipmentRowMapper;
    }

    @Override
    public void save(Equipment equipment) {
        jdbcTemplate.update(SQL_INSERT, equipment.getEquipmentName(), equipment.getWellId());
    }

    @Override
    public List<Equipment> findByWellId(int wellId) {
        return jdbcTemplate.query(SQL_FIND_BY_WELL_ID, equipmentRowMapper, wellId);
    }

    @Override
    public Equipment findByName(String name) {
        return jdbcTemplate.queryForObject(SQL_FIND_BY_NAME, equipmentRowMapper, name);
    }
}
