package com.company.wellapp.repositories;

import com.company.wellapp.models.entity.Equipment;

import java.util.List;

public interface EquipmentRepository {
    void save(Equipment equipment);

    List<Equipment> findByWellId(int wellId);

    Equipment findByName(String name);
}
