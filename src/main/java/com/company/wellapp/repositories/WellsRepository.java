package com.company.wellapp.repositories;

import com.company.wellapp.models.entity.Equipment;
import com.company.wellapp.models.entity.Well;

import java.util.List;

public interface WellsRepository {
    void save(Well well);
    Well findWellByName(String wellName);
    List<Well> findAllWells();
}
