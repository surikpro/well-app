package com.company.wellapp.repositories;

import com.company.wellapp.mappers.WellRowMapper;
import com.company.wellapp.models.entity.Well;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

// DAO class for Well Entity
@Component
public class WellsRepositoryImpl implements WellsRepository {
    //language=SQL
    private final String SQL_INSERT = "INSERT INTO well(name) VALUES (?)";
    private final String SQL_FIND_BY_NAME = "SELECT id, name FROM well WHERE name = ?";
    private final String SQL_FIND_ALL_WELLS = "SELECT * FROM well";

    private final JdbcTemplate jdbcTemplate;
    private final WellRowMapper wellRowMapper;

    @Autowired
    public WellsRepositoryImpl(DataSource dataSource, WellRowMapper wellRowMapper) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.wellRowMapper = wellRowMapper;
    }

    @Override
    public void save(Well well) {
        String name = well.getWellName();
        jdbcTemplate.update(SQL_INSERT, name);
    }

    @Override
    public List<Well> findAllWells() {
        return jdbcTemplate.query(SQL_FIND_ALL_WELLS, wellRowMapper);
    }

    @Override
    public Well findWellByName(String wellName) {
        try {
            return jdbcTemplate.queryForObject(SQL_FIND_BY_NAME, wellRowMapper, wellName);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }

    }
}
