package com.company.wellapp.exceptions;
// Custom Number Format Exception class
public class CustomNumberFormatException extends NumberFormatException {
    public CustomNumberFormatException() {
        super();
        System.out.println("Your entry is wrong. Please follow the requirements!");
    }
}
