package com.company.wellapp.exceptions;
// Custom Array Index Out Of Bounds Exception class
public class CustomArrayIndexOutOfBoundsException extends ArrayIndexOutOfBoundsException {
    public CustomArrayIndexOutOfBoundsException() {
        super();
        System.out.println("You exceeded the quantity of data input. Please follow the requirements!");
    }
}
