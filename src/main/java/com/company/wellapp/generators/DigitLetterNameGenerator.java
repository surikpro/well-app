package com.company.wellapp.generators;

import org.springframework.stereotype.Component;

import java.util.Random;

//Generates digits and letters for the name of equipments
@Component
public class DigitLetterNameGenerator implements Generator {
    private final static char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

    @Override
    public String generate(int length) {
        char[] generatedName = new char[length];
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < length; i++) {
            generatedName[i] = chars[random.nextInt(chars.length)];
        }
        return new String(generatedName);
    }
}
