package com.company.wellapp.generators;

public interface Generator {
    String generate(int length);
}
