package com.company.wellapp;

import com.company.wellapp.configuration.ApplicationConfiguration;
import com.company.wellapp.handlers.InputHandler;
import com.company.wellapp.handlers.InputHandlerImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Scanner;

// Entry Point of the Application
public class WellAppApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        Scanner scanner = new Scanner(System.in);
        InputHandler handler = applicationContext.getBean(InputHandlerImpl.class);
        handler.start(scanner);

    }

}
